#include <iostream>
#include <fstream>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include "random.h"
#include "rechercheTabou.h"
#include "solution.h"

using namespace std;

int main(int argc, char **argv)
// argc : parameter names
// argv : list containing the parameters
// Either the executable 'algo_tabou' takes no arguments or it takes 4 arguments
//   1. number of iterations (algo stop criterion)
//   2. duration of the Taboo list (length?)
//   3.  name of the cities
//   4. file name indicating distances between cities

{
	//initiates the random number generator
	Random::randomize();

	// default values
	int nb_iteration = 10;
	int duree_tabou  = 0;
	int nb_villes    = 10;
	char fileDistances[100];
	strcpy(fileDistances,"distances_entre_villes_10.txt");

	if (argc == 5)
	{
		nb_iteration = atoi(argv[1]);
		duree_tabou  = atoi(argv[2]);
		nb_villes    = atoi(argv[3]);
		strcpy(fileDistances,argv[4]);
	}
	else if (argc == 1)
	{
		cout << "Default parameters" << endl;
	}
	else
	{
		cout << "Some of the arguments are not correct." << endl;
		cout << "Either the executable 'algo_tabou' takes no arguments or it takes 4 arguments:" << endl;
		cout << "   1. number of iterations (integer > 0)" << endl;
		cout << "   2. length Taboo (integer > 0)" << endl;
		cout << "   3. number of cities (=size of the solution)" << endl;
		cout << "   4. file name indicating distances between cities" << endl;
		exit(EXIT_FAILURE);
	}

	// Initiates the parameters of the TabooSearch and creates the initial solution.
	rechercheTabou algo(nb_iteration, duree_tabou, nb_villes, fileDistances);
	//   1st parameter: number of iterations (algo stop criterion)
    //   2nd parameter: Taboo list duration
    //   3rd parameter: number of cities
    //   4th parameter: file containing the disantances between the cities.

	//  Start the search with the Taboo method
	solution* best = algo.optimiser();
	// Displays the best solution encountered
	cout << endl << "the best solution encountered is : ";
	best->afficher();

	delete best;

	return 0;
}
