#include "rechercheTabou.h"

using namespace std;

// Initialization of the parameters of the Taboo Search
// and generation of the initial solution.
// Initialization of the taboo list
rechercheTabou::rechercheTabou(int nbiter,int dt,int nv, char* nom_fichier)
{
  nbiterations    = nbiter;
  iter_courante   = 0;
  duree_tabou     = dt;
  taille_solution = nv;
  constuction_distance(taille_solution, nom_fichier);
  courant         = new solution(nv);
  courant->evaluer(les_distances);

  list_tabou = new int*[nv];
  for(int i=0; i<nv; i++)
    {
      list_tabou[i] = new int[nv];
      for(int j=0; j<nv; j++)
	list_tabou[i][j] = -1;
    }

  cout << "The initial random solution is    : ";
  courant->afficher();

  list_tabou2 = new int*[duree_tabou];
  for(int i=0; i<duree_tabou; i++)
    {
      list_tabou2[i] = new int[taille_solution];
      for(int j=0; j<taille_solution; j++)
	list_tabou2[i][j] = -1;
    }
}

rechercheTabou::~rechercheTabou()
{
  delete courant;
  for(int i=0; i<taille_solution; i++)
    {
      delete list_tabou[i];
      delete les_distances[i];
    }
  for(int i=0; i<duree_tabou; i++)
    delete list_tabou2[i];
  delete[] list_tabou;
  delete[] list_tabou2;
  delete[] les_distances;
}

void rechercheTabou::constuction_distance(int nv, char* nom_fichier)
{
  les_distances = new int*[nv];
  for(int i=0; i<nv; i++)
    les_distances[i] = new int[nv];

  ifstream fichier;
  // Opens the file of distances between cities
  fichier.open(nom_fichier, ifstream::in);
  if(!fichier.is_open())
    {
      cerr<<"File [" << nom_fichier << "] invalid."<<endl;
      exit(-1);
    }

  for (int i=0; i<nv; i++)
    {
      for(int j=i+1; j<nv; j++)
	{
	  fichier >> les_distances[i][j];
	  les_distances[j][i] = les_distances[i][j];
	}
    }

  for (int i=0; i<nv; i++)
    les_distances[i][i] = -10;

  fichier.close();
}

bool rechercheTabou::nonTabou(int i, int j)
{
  if(list_tabou[i][j]<iter_courante)
    return true;
  else
    return false;
}

bool rechercheTabou::nonTabou2(solution* sol)
{
  for(int i=0; i<duree_tabou; i++)
    {
      for(int j=1; j<taille_solution; j++)
	{
	  if (list_tabou2[i][j]!=sol->ville[j])
	    j = taille_solution;
	  else if (j == taille_solution-1)
	    return false;
	}
    }
  for(int i=0; i<duree_tabou; i++)
    {
      for(int j=1; j<taille_solution; j++)
	{
	  if (list_tabou2[i][j]!=sol->ville[taille_solution-j])
	    j = taille_solution;
	  else if (j == taille_solution-1)
	    return false;
	}
    }
  return true;
}

void rechercheTabou::mise_a_jour_liste_tabou_2(solution* sol, int&position)
{
  if (duree_tabou != 0)
    {
      for(int j=0; j<taille_solution; j++)
	list_tabou2[position][j] = sol->ville[j];
      position++;
      if (position == duree_tabou)
	position = 0;
    }
}

void rechercheTabou::voisinage_2_opt(int &best_i, int &best_j)
{
  int best_vois;
  bool tous_tabou = true;
  best_vois = 100000;

  // we select a first city for the movement
  for(int i=0;i<taille_solution;i++)
    {
      // we select a second city for the movement
      for(int j=i+1;j<taille_solution;j++)
	{
	  if(   ((i!=0)||(j!=taille_solution-1))
		&& ((i!=0)||(j!=taille_solution-2)) )
            {
	      // we transform the current solution to the neighbor
	      //    thanks to the movement defined by the city couple
	      courant->inversion_sequence_villes(i,j);
	      // this neighbor is estimated
	      courant->evaluer(les_distances);
	      // if this movement is not taboo and
	      // if this neighbor has the best fitness
	      // then this neighbor becomes the best uninhibited neighbor.
	      if(nonTabou(i,j) && courant->fitness<best_vois)
                //if(nonTabou2(courant) && courant->fitness<best_vois)
                {
		  best_vois  = courant->fitness;
		  best_i     = i;
		  best_j     = j;
		  tous_tabou = false;
                }
	      // we re-transform this neighbor into the current solution
	      courant->inversion_sequence_villes(i,j);
	      // we re-evaluate the current solution
	      courant->evaluer(les_distances);
            }
	}
    }
}

//main procedure of the search
solution* rechercheTabou::optimiser()
{
  bool first            = true; // indicates if this is the first time
  //         that we are in a local mimium
  bool descente         = false;// indicates if the current solution corresponds to a descent
  int ameliore_solution = -1;   // indicates the iteration where has improved teh solution
  int f_avant, f_apres;         // fitness values before and after an iteration

  // The best solution found (= smallest minium found) to keep
  solution* best_solution = new solution(taille_solution);


  int best_i    = 0;            // The couple (best_i, best_j) represents the best non taboo movement.
  int best_j    = 0;
  int best_eval = courant->fitness;
  f_avant       = 10000000;

  // As long as the limit number of iterations is not reached
  for(iter_courante=0; iter_courante<nbiterations; iter_courante++)
    {
      voisinage_2_opt(best_i, best_j);            // The 'voisinage_2_opt'(neighborhood_2_opt) function returns the best
      //   non taboo movement; it's the couple (best_i, best_j)
      courant->inversion_sequence_villes(best_i, best_j);
      //  We move the current solution thanks to this movement

      courant->ordonner();                        // We reorder the solution starting from 0
      courant->evaluer(les_distances);            // The new current solution is being evaluated

      f_apres = courant->fitness;                 // value of fitness after movement

      if(courant->fitness < best_eval)            // if we improve the smallest minimum encountered
	{                                           // then we save it in 'best_solution'
	  best_eval = courant->fitness;           // we update 'best_eval'.
	  best_solution->copier(courant);         // we save the corante solution as best_solution
	  best_solution->evaluer(les_distances);  // we evaluate the best solutionon
	  ameliore_solution = iter_courante;      // improvement is reported to have taken place at this iteration
	}
      else //  If we are not in the lowest minimum met but in a local minimum
	{
	  // Local minimum detection criteria. 2 cases:
	  //  1. if the new solution is worse than the old one
	  //         and we are in the process of a descent
	  //  2. if the new solution is identical to the old one 
	  //         and this is the first time this happens
	  if (    ((f_avant<f_apres)&&(descente==true))
		  || ((f_avant == f_apres)&&(first)) )
            {
	      
	      cout << "We are in a local minimum at iteration  "
		   << iter_courante-1 << " -> min = " << f_avant
		   << " km (the + small local min already found = "
		   << best_eval << " km)" << endl;
	      first = false;
            }

	  if (f_avant<=f_apres)  // the current solution degrades
	    descente = false;
	  else
	    descente = true;   // the current solution is getting better: descent

	  if ((f_avant!=f_apres)&&(!first)) //
	    first = true;



			
        }


      

      // update of the taboo list
      list_tabou[best_i][best_j] = iter_courante+duree_tabou;
      //update_taboo_list_2(curante, position);
      f_avant = f_apres; 

      // output: index of iteration and the optimal solution so far en C
      printf("%d\t%d\t%d\n", iter_courante, courant->fitness, best_eval);
    }
  return best_solution;
}
