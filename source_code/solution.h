
#ifndef SOLUTION_H
#define SOLUTION_H

#include <math.h>
#include <stdio.h>
#include <iostream>
#include "random.h"

// The solution class represents the structure
// of a solution to the problem of the traveling sales man.
class solution{
public:
	// ATTRIBUTS
	int *ville;                  // the variables of the solution
	int taille;                  // the size of the solution
	int fitness;                 // the fitness value of the solution = length of the loop

	// CONSTRUCTEURS
	solution(int nv);
	~solution();

	// METHODES
	void evaluer(int **distance);     // solution evaluation function (i.e. fitness calculation)
                                      //   It has to be launched at the creation of the solutions and afterwards
                                      //   execution of mutation and crossover operators
	void afficher();                  // function displaying the solution
	void ordonner();                  // orders the direction of the movement if city [1] > city [size-1]
	void copier(solution* source);    // copies the 'source' solution
	bool identique(solution* chro);   //  test if 2 solutions are identical
	void echange_2_villes(int ville1, int ville2);          // interchange 2 cities of the solution
	void inversion_sequence_villes(int ville1, int ville2); // reverses a sequence of cities in the solution
	void deplacement_1_ville(int ville1, int ville2);       // moves a city in the solution

};

# endif

