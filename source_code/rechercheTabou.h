
#ifndef _RT_H
#define _RT_H

#include <math.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <stdio.h>
#include "random.h"
#include "solution.h"

// this class defines the parameters of an execution
// of the taboo search as well as the main search procedure
class rechercheTabou{
public:
	// ATTRIBUTS
	int iter_courante;
	int nbiterations;              // number of iterations after which the search is stopped
	int duree_tabou;               // taboo duration in number of iterations
	int taille_solution;           // number of cities in the solution
	solution *courant;             // current solution managed by RechercheTabou
	int **list_tabou2;             // list of taboo solutions
	int **list_tabou;              // list of taboo durations associated with each city pair
	int **les_distances;           // matrix of distances between cities	                                                 // the neighborhood is in the neighborhood direction 2-opt

	// CONSTRUCTORS
	rechercheTabou(int nbiter, int dt, int nv, char* nom_fichier);  // construction of the researchTabou
	~rechercheTabou();

	// METHODES
	bool nonTabou(int i,int j);                      // the couple (ville i, ville j) is taboo: yes or no
	bool nonTabou2(solution* sol);                   // the 'sol' solution is taboo: yes or no
	void voisinage_2_opt(int& best_i, int &best_j);  // gives the best non taboo neighborhood
	solution* optimiser();                           // launch of researchTabou
	void constuction_distance(int nv, char* nom_fichier);
	void mise_a_jour_liste_tabou_2(solution* sol, int& position);
	                                                 // addition of the solution 'sol' to the 'position' position
	                                                 //  from the taboo list
};

# endif
