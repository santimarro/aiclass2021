#include "solution.h"

using namespace std;


// solution initialization: one cycle passing through all cities. 
solution::solution(int nv)
{
	int a;
	bool recommence = true;
	taille          = nv;
	ville           = new int[taille];
    //  Randomly, we choose to start with the city 0
	ville[0]        = 0;
	for(int i=1; i<taille; i++)
	{
		recommence  = true;
		while(recommence)
		{
			recommence = false;
			// we randomly select the next city
			a = Random::aleatoire(taille);
			// the city must not be already in the cycle
			// if this is the case, we start again
			for (int j=0; j<i; j++)
				if (a==ville[j])
					recommence = true;
		}
		ville[i] = a;
	}
	// it is arbitrarily imposed that city [1] > city [size-1]
	ordonner();
}

solution::~solution()
{
	delete[] ville;
}

// solution evaluation: the sum of the distances between the cities
void solution::evaluer(int **distance)
{
	fitness = 0;
	for(int i=0;i<taille-1;i++)
		fitness += distance[ville[i]][ville[i+1]];
	fitness+=distance[ville[0]][ville[taille-1]];
}

void solution::afficher()
{
	for(int i=0;i<taille;i++)
		cout << ville[i] << "-";
	cout << "--> " << fitness << " km" << endl;
}

// it is arbitrarily imposed that the 2nd city visited (ville[1])
// has a smaller number than the last city visited (ville[taille-1])
//   i.e. : ville[1] > ville[taille-1]
void solution::ordonner()
{
	int inter, k;
	// Places the city "0" at the head of the solution (ville[0])
	if (ville[0] != 0)
	{
		int position_0 = 0;
		int * ville_c  = new int[taille];
		for (int i=0; i<taille; i++)
		{
			ville_c[i] = ville[i];
			if (ville[i]==0)
				position_0 = i;
		}
		k = 0;
		for (int i=position_0; i<taille; i++)
		{
			ville[k] = ville_c[i];
			k++;
		}
		for (int i=0; i<position_0; i++)
		{
			ville[k] = ville_c[i];
			k++;
		}
		delete[] ville_c;
	}

	// The number of the 2nd city must be smaller than the number of the last city.
	if (ville[1] > ville[taille-1])
	{
		for(int k=1; k<=1+(taille-2)/2; k++)
		{
			inter = ville[k];
			ville[k] = ville[taille-k];
			ville[taille-k] = inter;
		}
	}
}

// we exchange 2 cities in the solution
void solution::echange_2_villes(int ville1, int ville2)
{
	int inter    = ville[ville1];
	ville[ville1] = ville[ville2];
	ville[ville2] = inter;
}

void solution::deplacement_1_ville(int ville1, int ville2)
{
	int inter = ville[ville1];

	// on transforme la solution courante vers le voisin
	//    gr�ce au mouvement d�finit par le couple de ville
	if (ville1 < ville2)
	{
	    for (int k=ville1; k<ville2; k++)
            ville[k] = ville[k+1];
	}
	else
	{
	    for (int k=ville1; k>ville2; k--)
            ville[k] = ville[k-1];
	}
    ville[ville2] = inter;
}


// we reverse a whole sequence of cities in the solution
void solution::inversion_sequence_villes(int ville1, int ville2)
{
	for(int k=ville1; k<=ville1+(ville2-ville1)/2; k++)
	    echange_2_villes(k, ville2+ville1-k);
}

bool solution::identique(solution* chro)
{
	for(int i=1; i<taille; i++)
		if (chro->ville [i] != this->ville[i])
			return false;
	return true;
}

// copie les villes d'une solution. la fitness n'est reprise
// copies the cities of a solution. fitness is not taken over
void solution::copier(solution* source)
{
	for(int i=0; i<taille; i++)
		ville[i] = source->ville[i];
}

