#ifndef _RANDOM_H
#define _RANDOM_H

#include <stdlib.h>
#include <time.h>

class Random{
public:
    // initialize the random variable generator
    static void randomize()
    {
      srand ( time(NULL) );
    };

    // return a random variable between 0 and borne - 1
    static long aleatoire(long borne)
    {
      return(rand()%borne);
    };
};

# endif
